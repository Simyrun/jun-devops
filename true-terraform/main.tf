
data "aws_availability_zones" "available" {}


#--------------------------------------------------------------
resource "aws_security_group" "web" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    for_each = ["80", "443", "22"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Dynamic SecurityGroup"
    Owner = "Egor Gruzdev"
  }
}




resource "aws_launch_configuration" "as_conf" {
  name_prefix     = "terraform-lc-example-"
  image_id        = data.aws_ami.ubuntu.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web.id]
  //user_data       = file("user_data.sh")
  user_data = file("user_ssh.sh")
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "Gruzdev-autoscaling" {
  name                 = "terraform-asg-example"
  launch_configuration = aws_launch_configuration.as_conf.name
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 2
  vpc_zone_identifier  = [aws_default_subnet.primary.id, aws_default_subnet.secondary.id]
  health_check_type    = "ELB"
  load_balancers       = [aws_elb.web-gruzdev-elb.name]

  dynamic "tag" {
    for_each = {
      Name   = "WebServer in ASG"
      Owner  = "Egor Gruzdev"
      TAGKEY = "TAGVALUE"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "web-gruzdev-elb" {
  name               = "web-gruzdev-elb"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web.id]


  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 10
    unhealthy_threshold = 10
    timeout             = 10
    target              = "HTTP:80/"
    interval            = 50
  }

}






# e.g. Create subnets in the first two available availability zones

resource "aws_default_subnet" "primary" {
  availability_zone = data.aws_availability_zones.available.names[0]

}

resource "aws_default_subnet" "secondary" {
  availability_zone = data.aws_availability_zones.available.names[1]

}

#--------------------------------------------------
output "web_loadbalancer_url" {
  value = aws_elb.web-gruzdev-elb.dns_name
}
