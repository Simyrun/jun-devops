#!/bin/bash
#Create user for connecting Ansible
NEW_USER=ansible

adduser --disabled-password $NEW_USER
echo ''$NEW_USER'  ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers.d/$NEW_USER
su  $NEW_USER
cd /home/$NEW_USER

#
mkdir -p .ssh

chmod 0700 .ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDre5XFXoXlXLOn7R33f8hTAZ2hUH0wP5yYengK8HkJF6jiId+NU44CxvXKhVLtwluty4SHxMvQLmUIfxMf1lB5JX/TTl0Wt1yIzBTzWceKTCtHg7r7PrVE8vN81ukUrW2yGADrlDddLpUM2tjjvbcMS2U8Zv2Md6zEsrltQXvmGhekU/fi50MfXC7a5dGcQrcT6mrDu1x7qOTMFTfrc/Qt/R/uLj0Kgb9ze6bNgwo+40+4pkscUMzi+184p3jai0MVi7wAiozlpvlgmDmeKwEtXKTyffcuZjku9AB5fahjLq/3g5j+opAfRKT8GwBXXkGAXg2K5svdn589spC1csuMqbngibXmlRoQqTry4404ZUcJzRto2W/wHYGuKUTRKKujJxZeDx+j4dgD3/ywffD5xL/FBypN6h1Q03HOcWN92s5PK9TANsaltsbEBZCOPxzwqQdXQyYgBrSUvXv0Aic7ohe+VwXJknmXXudjLDfUs7nAieIoVdU6Yjm0ZX68rZDykgELOLF3Rlry1uhDabgUKEt2jhs858Kwy7pI+ShLxXSdoa7VEjKDmozlyQOfjGP82ukyB+VNFltm0P78EcrrY5SqqbHP9IvbR96y4QCvTDWSm66BNHFEPdAe24LOR5VlSbm7pDBfk56aPKq5W47V/ypdStXcsc0H6rPgXJE62w== simyrun@simyrun-laptop" > .ssh/authorized_keys
chmod 644 .ssh/authorized_keys
chown -R $NEW_USER .ssh


apt update
apt install nginx -y
systemctl start nginx
systemctl enable nginx

#echo "Hello, World" > index.html

#nohup busybox httpd -f -p 8080 &

##cloud-config
#cloud_final_modules:
#- [users-groups,always]
#users:
#  - name: ansible
#    groups: sudo
#    shell: /bin/bash
#    ssh-authorized-keys:
#    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDre5XFXoXlXLOn7R33f8hTAZ2hUH0wP5yYengK8HkJF6jiId+NU44CxvXKhVLtwluty4SHxMvQLmUIfxMf1lB5JX/TTl0Wt1yIzBTzWceKTCtHg7r7PrVE8vN81ukUrW2yGADrlDddLpUM2tjjvbcMS2U8Zv2Md6zEsrltQXvmGhekU/fi50MfXC7a5dGcQrcT6mrDu1x7qOTMFTfrc/Qt/R/uLj0Kgb9ze6bNgwo+40+4pkscUMzi+184p3jai0MVi7wAiozlpvlgmDmeKwEtXKTyffcuZjku9AB5fahjLq/3g5j+opAfRKT8GwBXXkGAXg2K5svdn589spC1csuMqbngibXmlRoQqTry4404ZUcJzRto2W/wHYGuKUTRKKujJxZeDx+j4dgD3/ywffD5xL/FBypN6h1Q03HOcWN92s5PK9TANsaltsbEBZCOPxzwqQdXQyYgBrSUvXv0Aic7ohe+VwXJknmXXudjLDfUs7nAieIoVdU6Yjm0ZX68rZDykgELOLF3Rlry1uhDabgUKEt2jhs858Kwy7pI+ShLxXSdoa7VEjKDmozlyQOfjGP82ukyB+VNFltm0P78EcrrY5SqqbHP9IvbR96y4QCvTDWSm66BNHFEPdAe24LOR5VlSbm7pDBfk56aPKq5W47V/ypdStXcsc0H6rPgXJE62w== simyrun@simyrun-laptop
